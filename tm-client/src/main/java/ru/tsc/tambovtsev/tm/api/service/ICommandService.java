package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(@NotNull AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull Iterable<AbstractCommand> getCommandsWithArgument();

}