package ru.tsc.tambovtsev.tm.dynamicsql.provider;

import org.mybatis.dynamic.sql.SqlColumn;
import ru.tsc.tambovtsev.tm.dynamicsql.model.TaskTable;
import ru.tsc.tambovtsev.tm.enumerated.Status;

public class TaskProvider {

    public static final TaskTable task = new TaskTable();

    public static final SqlColumn<String> id = task.id;

    public static final SqlColumn<String> userId = task.userId;

    public static final SqlColumn<String> name = task.name;

    public static final SqlColumn<String> description = task.description;

    public static final SqlColumn<Status> status = task.status;

    public static final SqlColumn<String> projectId = task.projectId;

}
