package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IProjectTaskService;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @Nullable
    private final IProjectRepository projectRepository;

    @Nullable
    private final ITaskRepository taskRepository;

    @Nullable
    private final IConnectionService connectionService;

    public ProjectTaskService(
            @Nullable final IProjectRepository projectRepository,
            @Nullable final ITaskRepository taskRepository,
            @Nullable IConnectionService connectionService
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable final Task task = repository.findById(userId, taskId);
            if (task == null) return;
            task.setProjectId(projectId);
            repository.updateById(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable final Task task = repository.findById(userId, taskId);
            if (task == null) return;
            task.setProjectId(null);
            repository.updateById(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            taskRepository.removeByProjectId(userId, projectId);
            projectRepository.removeById(projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
