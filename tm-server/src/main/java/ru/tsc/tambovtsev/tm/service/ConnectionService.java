package ru.tsc.tambovtsev.tm.service;

import org.apache.ibatis.datasource.unpooled.UnpooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.repository.*;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IDatabaseProperty;
import ru.tsc.tambovtsev.tm.handler.RoleTypeHandler;
import ru.tsc.tambovtsev.tm.handler.StatusTypeHandler;

import javax.sql.DataSource;

public class ConnectionService implements IConnectionService {

    @NotNull
    final IDatabaseProperty databaseProperties;

    public ConnectionService (@NotNull IDatabaseProperty databaseProperties) {
        this.databaseProperties = databaseProperties;
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() { return getSqlSessionFactory().openSession(); }

    @NotNull
    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String username = databaseProperties.getDatabaseUsername();
        @NotNull final String password = databaseProperties.getDatabasePassword();
        @NotNull final String url = databaseProperties.getDatabaseUrl();
        @NotNull final String driver = databaseProperties.getDatabaseDriver();
        final DataSource dataSource = new UnpooledDataSource(driver, url, username, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IDomainRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(RoleTypeHandler.class);
        configuration.addMapper(StatusTypeHandler.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
