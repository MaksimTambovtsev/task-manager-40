package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

public interface IOwnerRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@Nullable String userId);

    @Nullable
    M findById(@Nullable String userId, @Nullable String id);

    int getSize(@Nullable String userId);

    @Nullable
    void removeById(@Nullable String userId, @Nullable String id);

}
