package ru.tsc.tambovtsev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.tsc.tambovtsev.tm.handler.StatusTypeHandler;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IOwnerRepository<Project> {

    @Insert("INSERT INTO TM_PROJECT (ID, NAME, DESCRIPTION, STATUS, USER_ID) VALUES " +
            "(#{id}, #{name}, #{description}, #{status}, #{userId})"
    )
    void addAll(@NotNull Collection<Project> models);

    @Insert("INSERT INTO TM_PROJECT (ID, NAME, DESCRIPTION, STATUS, USER_ID) VALUES " +
            "(#{id}, #{name}, #{description}, #{status}, #{userId})"
    )
    void create(@NotNull Project project);

    @Update(
            "UPDATE TM_PROJECT SET " +
                    "NAME = #{name}, DESCRIPTION = #{description}, STATUS = #{status}, " +
                    "USER_ID = #{userId} WHERE ID = #{id}"
    )
    void updateById(@NotNull Project project);

    @Nullable
    @Select("SELECT * FROM TM_PROJECT WHERE ID = #{id} AND USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "userId", column = "USER_ID")
    })
    Project findById(@Param("userId") @Nullable String userId, @Param ("id") @Nullable String id);

    @Delete("DELETE FROM TM_PROJECT WHERE USER_ID = #{userId}")
    void clear(@Nullable String userId);

    @Select("SELECT COUNT(1) FROM TM_PROJECT WHERE USER_ID = #{userId}")
    int getSize(@Nullable String userId);

    @Nullable
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<Project> findAll(SelectStatementProvider selectStatement);

    @Nullable
    @Select("SELECT * FROM TM_PROJECT")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "userId", column = "USER_ID")
    })
    List<Project> findAllProject();

    @Delete("DELETE FROM TM_PROJECT WHERE USER_ID = #{userId} AND ID = #{id}")
    void removeByIdProject(@Param ("userId") @Nullable String userId, @Param ("id") @Nullable String id);

    @Delete("DELETE FROM TM_PROJECT WHERE ID = #{id}")
    void removeById(@Nullable String id);

    @Delete("DELETE FROM TM_PROJECT")
    void clearProject();

}