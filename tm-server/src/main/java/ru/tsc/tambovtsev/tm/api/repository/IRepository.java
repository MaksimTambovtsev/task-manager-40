package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractEntity> {

    @Nullable
    M findById(@Nullable String id);

    @Nullable
    void removeById(@Nullable String id);

    void addAll(@NotNull Collection<M> models);

}
