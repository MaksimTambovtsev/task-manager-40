package ru.tsc.tambovtsev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.handler.RoleTypeHandler;
import ru.tsc.tambovtsev.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Insert("INSERT INTO TM_USER (ID, LOGIN, EMAIL, PASSWORD_HASH, FIRSTNAME, LASTNAME, MIDDLENAME, ROLE, LOCKED) VALUES " +
            "(#{id}, #{login}, #{email}, #{passwordHash}, #{firstName}, #{lastName}, #{middleName}, #{role}, 'false')"
    )
    void addAll(@NotNull Collection<User> models);

    @Insert("INSERT INTO TM_USER (ID, LOGIN, EMAIL, PASSWORD_HASH, FIRSTNAME, LASTNAME, MIDDLENAME, ROLE, LOCKED) VALUES " +
    "(#{id}, #{login}, #{email}, #{passwordHash}, #{firstName}, #{lastName}, #{middleName}, #{role}, 'false')"
    )
    void create(@Nullable User user);

    @Update("UPDATE TM_USER SET LOCKED = 'true' WHERE ID = #{id}")
    void lockUserById(@Param ("id") @NotNull String id);

    @Update("UPDATE TM_USER SET LOCKED = 'false' WHERE ID = #{id}")
    void unlockUserById(@Param ("id") @NotNull String id);

    @Update(
            "UPDATE TM_USER SET " +
                    "FIRSTNAME = #{firstName}, LASTNAME = #{lastName}, MIDDLENAME = #{middleName}, " +
                    "PASSWORD_HASH = #{passwordHash} WHERE ID = #{id}"
    )
    void updateUser(@NotNull User user);

    @Nullable
    @Select("SELECT * FROM TM_USER WHERE LOGIN = #{login}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRSTNAME"),
            @Result(property = "lastName", column = "LASTNAME"),
            @Result(property = "middleName", column = "MIDDLENAME"),
            @Result(property = "role", column = "ROLE", typeHandler = RoleTypeHandler.class),
            @Result(property = "locked", column = "LOCKED"),
    })
    User findByLogin(@Param ("login") @Nullable String login);

    @Nullable
    @Select("SELECT * FROM TM_USER WHERE EMAIL = #{email}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRSTNAME"),
            @Result(property = "lastName", column = "LASTNAME"),
            @Result(property = "middleName", column = "MIDDLENAME"),
            @Result(property = "role", column = "ROLE", typeHandler = RoleTypeHandler.class),
            @Result(property = "locked", column = "LOCKED"),
    })
    User findByEmail(@Param ("email") @Nullable String email);

    @Delete("DELETE FROM TM_USER WHERE LOGIN = #{login}")
    void removeByLogin(@Param ("login") @Nullable String login);

    @Nullable
    @Select("SELECT * FROM TM_USER WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRSTNAME"),
            @Result(property = "lastName", column = "LASTNAME"),
            @Result(property = "middleName", column = "MIDDLENAME"),
            @Result(property = "role", column = "ROLE", typeHandler = RoleTypeHandler.class),
            @Result(property = "locked", column = "LOCKED"),
    })
    User findById(@Param ("id") @Nullable String id);

    @Nullable
    @Select("SELECT * FROM TM_USER")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRSTNAME"),
            @Result(property = "lastName", column = "LASTNAME"),
            @Result(property = "middleName", column = "MIDDLENAME"),
            @Result(property = "role", column = "ROLE", typeHandler = RoleTypeHandler.class),
            @Result(property = "locked", column = "LOCKED"),
    })
    List<User> findAllUser();

    @Delete("DELETE FROM TM_USER")
    void clearUser();

}
