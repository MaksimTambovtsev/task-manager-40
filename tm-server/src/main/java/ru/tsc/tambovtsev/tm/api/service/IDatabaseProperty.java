package ru.tsc.tambovtsev.tm.api.service;

import java.util.Properties;

public interface IDatabaseProperty {

    Properties properties = new Properties();

    default String getDatabaseUsername() {
        return properties.getProperty("database.username", "root");
    }

    default String getDatabasePassword(){
        return properties.getProperty("database.password", "sadmin");
    }

    default String getDatabaseUrl(){
        return properties.getProperty("database.url",
                "jdbc:mysql://localhost:3306/example?createDatabaseIfNotExist=true");
    }

    default String getDatabaseDriver() {
        return properties.getProperty("database.driver", "com.mysql.cj.jdbc.Driver");
    }

}
