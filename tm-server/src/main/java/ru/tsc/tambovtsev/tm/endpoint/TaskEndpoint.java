package ru.tsc.tambovtsev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.tambovtsev.tm.api.service.IProjectTaskService;
import ru.tsc.tambovtsev.tm.api.service.IServiceLocator;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.dto.request.*;
import ru.tsc.tambovtsev.tm.dto.response.*;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Session;
import ru.tsc.tambovtsev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSortType();
        return new TaskListResponse(getTaskService().findAll(userId,sort));
    }

    @NotNull
    @WebMethod
    public TaskSizeResponse sizeTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskSizeRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final TaskSizeResponse response = new TaskSizeResponse();
        response.setSize(getTaskService().getSize(userId));
        return response;
    }

    @NotNull
    @WebMethod
    public TaskBindProjectResponse bindProjectTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindProjectRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final IProjectTaskService projectTaskService = getServiceLocator().getProjectTaskService();
        projectTaskService.bindTaskToProject(userId, projectId, id);
        return new TaskBindProjectResponse();
    }

    @NotNull
    @WebMethod
    public TaskUnbindProjectResponse unbindProjectTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindProjectRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final IProjectTaskService projectTaskService = getServiceLocator().getProjectTaskService();
        projectTaskService.unbindTaskFromProject(userId, id);
        return new TaskUnbindProjectResponse();
    }

    @NotNull
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(null);
    }

    @NotNull
    @WebMethod
    public TaskShowByIdResponse showTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().findById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

}
