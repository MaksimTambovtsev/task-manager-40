package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;

public final class TaskServiceTest {
/*
    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    @Before
    public void setTaskService() {
        taskService.create("123", "432", "222");
        taskService.create("1321", "234", "555");
    }

    @After
    public void clearTaskService() {
        taskService.clear();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(taskService.findAll().isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(taskService.findAll().get(0).getName().isEmpty());
    }

    @Test
    public void testFindById() {
        @Nullable final List<Task> tasks = taskService.findAll();
        @Nullable final String taskId = tasks.stream().findFirst().get().getId();
        Assert.assertFalse(taskService.findById(taskId).getName().isEmpty());
    }

    @Test
    public void testRemove() {
        @NotNull final Task task = taskService.findAll().get(1);
        taskService.remove(task);
        Assert.assertNotEquals(task.getId(), taskService.findAll().get(0).getId());
    }

    @Test
    public void testRemoveById() {
        @NotNull final Task task = taskService.findAll().get(1);
        taskService.removeById(task.getId());
        Assert.assertNotEquals(task.getId(), taskService.findAll().get(0).getId());
    }

    @Test
    public void testExistById() {
        @NotNull final Task taskFirst = taskService.findAll().get(0);
        Assert.assertTrue(taskService.existsById(taskFirst.getId()));
    }

    @Test
    public void testUpdateById() {
        @NotNull final Task task = taskService.findAll().get(0);
        taskService.updateById(task.getUserId(), task.getId(), "Project1", "Descriptions");
        Assert.assertEquals(taskService.findById(task.getId()).getName(), "Project1");
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByIdNegative() {
        @NotNull final Task task = taskService.findAll().get(0);
        taskService.updateById(task.getUserId(), null, "Project1", "Descriptions");
        Assert.assertEquals(taskService.findById(task.getId()).getName(), "Project1");
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Task task = taskService.findAll().get(0);
        taskService.changeTaskStatusById(task.getUserId(), task.getId(), Status.COMPLETED);
        Assert.assertEquals(taskService.findById(task.getId()).getStatus(), Status.COMPLETED);
    }

    @Test
    public void testClear() {
        taskService.clear();
        Assert.assertTrue(taskService.findAll().isEmpty());
    }
*/
}
