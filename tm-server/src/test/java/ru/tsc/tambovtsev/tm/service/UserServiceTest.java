package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.exception.field.LoginEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.NameEmptyException;
import ru.tsc.tambovtsev.tm.model.User;

public final class UserServiceTest {
/*
    private final IUserRepository userRepository = new UserRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IPropertyService propertyService = new PropertyService();
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            taskRepository,
            projectRepository
    );

    @Before
    public void setUserService() {
        userService.create("testUser1", "1212325346", "service@test.ru");
        userService.create("testUser2", "0394560983405968349568", "service2@test.ru");
    }

    @After
    public void clearUserService() {
        userService.clear();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(userService.findAll().isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(userService.findAll().get(0).getId().isEmpty());
    }

    @Test
    public void testFindByLogin() {
        @Nullable final User findUser =  userService.findByLogin("testUser1");
        Assert.assertFalse(findUser.getLogin().isEmpty());
    }

    @Test
    public void testFindByEmail() {
        @Nullable final User findUser =  userService.findByEmail("service@test.ru");
        Assert.assertFalse(findUser.getEmail().isEmpty());
    }

    @Test
    public void testRemoveById() {
        @Nullable final User findUser =  userService.findAll().get(0);
        userService.removeById(findUser.getId());
        Assert.assertNotEquals(userService.findAll().get(0).getId(), findUser.getId());
    }

    @Test
    public void testRemoveByLogin() {
        @Nullable final User findUser =  userService.findAll().get(0);
        userService.removeByLogin(findUser.getLogin());
        Assert.assertNotEquals(userService.findAll().get(0).getId(), findUser.getId());
    }

    @Test
    public void testExistByLogin() {
        @Nullable final User findUser =  userService.findAll().get(0);
        Assert.assertTrue(userService.isLoginExists(findUser.getLogin()));
    }

    @Test
    public void testUpdateByUserNegative() {
        @Nullable final User findUser =  userService.findAll().get(0);
        Assert.assertThrows(NameEmptyException.class, () ->
                userService.updateUser(findUser.getId(), null, "wWw", "rew"));
    }

    @Test
    public void testSetPassword() {
        @Nullable final User user = new User();
        user.setPasswordHash(userService.findAll().get(0).getPasswordHash());
        user.setEmail(userService.findAll().get(0).getEmail());
        user.setId(userService.findAll().get(0).getId());
        user.setLogin(userService.findAll().get(0).getLogin());
        userService.setPassword(user.getId(), userService.findAll().get(1).getPasswordHash());
        Assert.assertNotEquals(user.getPasswordHash(), userService.findAll().get(0).getPasswordHash());
    }

    @Test
    public void testCreateNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, ""));
    }

    @Test
    public void testClear() {
        userService.clear();
        Assert.assertTrue(userService.findAll().isEmpty());
    }
*/
}
