package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Task;

@NoArgsConstructor
public final class TaskUnbindProjectResponse extends AbstractTaskResponse {

    public TaskUnbindProjectResponse(@Nullable Task task) {
        super(task);
    }

}
